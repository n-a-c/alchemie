--[[
Alchemie mod
by nac
Version: alpha-0.6
License: GNU General Public License v3.0
]]

alchemie = {
  mod = "alchemie",
  version = "alpha-0.5",
  path = minetest.get_modpath("alchemie"),
}

local modpath = core.get_modpath("alchemie")

-- Include farming_plus API
local plus = minetest.get_modpath("farming_plus")
local farming = minetest.get_modpath("farming_plus")

-- Include Bonemeal API
local bonemeal = minetest.get_modpath("bonemeal")

-- Include files
dofile(modpath .. "/craft.lua")
dofile(modpath .. "/plants/aloe_vera.lua")
dofile(modpath .. "/plants/arnika.lua")
dofile(modpath .. "/ores/magnesium.lua")
dofile(modpath .. "/ores/magic_ore.lua")
dofile(modpath .. "/essence.lua")

-- Logging
minetest.log("action", "[MOD] alchemie mod loaded")

-- Node Lists
--[[
local dirt_NodesList = {
  "default:dirt",
  "default:dirt_with_coniferous_litter",
  "default:dirt_with_dry_grass",
  "default:dirt_with_grass",
  "default:dirt_with_rainforest_litter",
  "default:dirt_with_snow",
  "default:dry_dirt",
  "default:dry_dirt_with_dry_grass"
}
]]
