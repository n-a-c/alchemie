# Alchemie Mod


## Install
Clone the Repository to ~/.minetest/mods/
> git clone https://gitlab.com/n-a-c/alchemie.git


Zip Download:
> https://gitlab.com/n-a-c/alchemie/-/archive/master/alchemie-master.zip


### Dependencys
- Farming Redo (https://content.minetest.net/packages/TenPlus1/farming/)
- Bonemeal (https://content.minetest.net/packages/TenPlus1/bonemeal/)


## Changelog


### Version alpha-0.6
- add magic ore, lump, ingot + textures
- add essences dirt, air, fire, water
  - missing textures
- little bugfix by magnesium ore


### Version alpha-0.5.1
- bugfix for Plant "Arnika" and "Aloe Vera"
  - fix drop Items
- Craftitems now in Plants *.lua


### Version alpha-0.5
- add new Texture for "kleiner Heiltrank" with default Glass Bottle with color
- add example textures for Plant "Arnika"
- little bugfix Aloe Vera supported now bonemeal


### Version alpha-0.4.1
- add Textures for Magnesium Ore and Powder
- little bugfix by Magnesium Ore


### Version alpha-0.4
- add Ore "Magnesium"
- add Item "Magnesium Powder"


### Version alpha-0.3
- little Bugfixes
- Add more Textures


### Version alpha-0.2
- use Farming Redo Api


### Version alpha-0.1
- add Plant "Aloe Vera"
- add Item "kleiner Heiltrank" + Recipes
