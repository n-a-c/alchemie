--[[
magnesium.lua
]]

minetest.register_node("alchemie:magnesium_ore", {
  description = "Magnesium Erz",
  tiles = {"default_stone.png^alchemie_magnesium_ore.png"},
  groups = {cracky = 3, stone = 1},
  drop = "alchemie:magnesium_powder",
  sounds = default.node_sound_stone_defaults()
})

minetest.register_ore({
  ore_type = "scatter",
  ore = "alchemie:magnesium_ore",
  wherein = "default:stone",
  clust_scarcity = 10*10*10,
  clust_num_ores = 6,
  clust_size = 5,
  y_min = -31000,
  y_max = -5,
})

-- Craftitems
minetest.register_craftitem("alchemie:magnesium_powder", {
  description = "Magnesium Pulver",
  inventory_image = "alchemie_magnesium_powder.png"
})
 
