--[[
magic_ore.lua
]]


minetest.register_node("alchemie:magic_ore", {
  description = "Magisches Erz",
  tiles = {"default_stone.png^alchemie_magic_ore.png"},
  groups = {cracky = 3, stone = 1},
  drop = "alchemie:magic_ore_lump",
  sounds = default.node_sound_stone_defaults()
})

minetest.register_ore({
  ore_type = "scatter",
  ore = "alchemie:magic_ore",
  wherein = "default:stone",
  clust_scarcity = 10*10*10,
  clust_num_ores = 6,
  clust_size = 5,
  y_min = 10,
  y_max = 30000,
})

-- Craftitems
minetest.register_craftitem("alchemie:magic_ore_lump", {
  description = "Magischer Erzklumpen",
  inventory_image = "alchemie_magic_ore_lump.png"
})

minetest.register_craftitem("alchemie:magic_ore_ingot", {
  description = "Magischer Erzbarren",
  inventory_image = "alchemie_magic_ore_ingot.png"
})

minetest.register_craft({
  type = "cooking",
  output = "alchemie:magic_ore_ingot",
  recipe = "alchemie:magic_ore_lump",
  cooktime = "30"
})
