--[[
craft.lua
Crafting Recipes & Craft items for alchemie Mod
]]

minetest.register_craftitem("alchemie:wundverband", {
  description = "Wundverband",
  inventory_image = "alchemie_wundverband.png",
  on_use = minetest.item_eat(2),
})

minetest.register_craftitem("alchemie:aloevera_wundverband", {
  description = "Aloe Vera Wundverband",
  inventory_image = "alchemie_aloevera_wundverband.png",
  on_use = minetest.item_eat(8)
})

minetest.register_craftitem("alchemie:aloevera_lotion", {
  description = "Aloe Vera Gel",
  inventory_image = "alchemie_aloevera_lotion",
})

minetest.register_craft({
  output = "alchemie:aloevera_lotion",
  recipe = {
    {"alchemie:alchemie_aloevera_leaf"}
  }
})

minetest.register_craft({
  output = "alchemie:wundverband",
  recipe = {
    {"farming:cotton"},
  }
})

minetest.register_craft({
  type = "shaped",
  output = "alchemie:aloevera_wundverband",
  recipe = {
    {"", "", ""},
    {"", "farming:cotton", ""},
    {"", "alchemie:wundverband", ""}
  }
})

-- Essences
minetest.register_craft({
  type = "shaped",
  output = "alchemie:dirt_essence",
  recipe = {
    {"default:dirt", "default:dirt", "default:dirt"},
    {"default:dirt", "alchemie:magic_ore_ingot", "default:dirt"},
    {"default:dirt", "default:dirt", "default:dirt"}
  }
})

minetest.register_craft({
  type = "shaped",
  output = "alchemie:air_essence",
  recipe = {
    {"default:air", "default:air", "default:air"},
    {"default:air", "alchemie:magic_ore_ingot", "default:air"},
    {"default:air", "default:air", "default:air"}
  }
})

minetest.register_craft({
  type = "shaped",
  output = "alchemie:fire_essence",
  recipe = {
    {"bucket:bucket_lava", "bucket:bucket_lava", "bucket:bucket_lava"},
    {"bucket:bucket_lava", "alchemie:magic_ore_ingot", "bucket:bucket_lava"},
    {"bucket:bucket_lava", "bucket:bucket_lava", "bucket:bucket_lava"}
  }
})

minetest.register_craft({
  type = "shaped",
  output = "alchemie:water_essence",
  recipe = {
    {"bucket:bucket_water", "bucket:bucket_water", "bucket:bucket_water"},
    {"bucket:bucket_water", "alchemie:magic_ore_ingot", "bucket:bucket_water"},
    {"bucket:bucket_water", "bucket:bucket_water", "bucket:bucket_water"}
  }
})
