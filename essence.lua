--[[
essence.lua

]]


-- Basic Essences
minetest.register_craftitem("alchemie:dirt_essence", {
  description = "Erd Essenz",
  inventory_image = "alchemie_dirt_essence.png"
})

minetest.register_craftitem("alchemie:air_essence", {
  description = "Luft Essenz",
  inventory_image = "alchemie_air_essence.png"
})

minetest.register_craftitem("alchemie:fire_essence", {
  description = "Feuer Essenz",
  inventory_image = "alchemie_fire_essence.png"
})

minetest.register_craftitem("alchemie:water_essence", {
  description = "Wasser Essenz",
  inventory_image = "alchemie_water_essence.png"
})
