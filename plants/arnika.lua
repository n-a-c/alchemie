--[[
arnika.lua
Arnika Plant
]]

minetest.register_craftitem("alchemie:alchemie_arnika_seed", {
  description = "Arnika Samen",
  inventory_image = "alchemie_arnika_seed.png",
  groups = {seed = 2, food_melon_slice = 1, flammable = 3},
  on_place = function(itemstack, placer, pointed_thing)
    return farming.place_seed(itemstack, placer, pointed_thing, "alchemie:arnika_1")
  end,
  on_use = minetest.item_eat(2),
})

minetest.register_craftitem("alchemie:alchemie_arnika_blossom", {
  description = "Arnika Blüte",
  inventory_image = "alchemie_arnika_blossom.png",
  groups = {seed = 2, food_tomato = 1, flammable = 2},
  on_use = minetest.item_eat(2),
})

local crop_def = {
  drawtype = "plantlike",
  tiles = {"alchemie_arnika_1.png"},
  paramtype = "light",
	sunlight_propagates = true,
	walkable = false,
	buildable_to = true,
	drop = "",
	selection_box = farming.select,
	groups = {
		snappy = 3, flammable = 2, plant = 1, attached_node = 1,
		not_in_creative_inventory = 1, growing = 1
	},
	sounds = default.node_sound_leaves_defaults()
}

-- stage 1
minetest.register_node("alchemie:arnika_1", table.copy(crop_def))

-- stage 2
crop_def.tiles = {"alchemie_arnika_2.png"}
minetest.register_node("alchemie:arnika_2", table.copy(crop_def))

-- stage 3
crop_def.tiles = {"alchemie_arnika_3.png"}
minetest.register_node("alchemie:arnika_3", table.copy(crop_def))

-- stage 4
crop_def.tiles = {"alchemie_arnika_4.png"}
crop_def.drop = {
  items = {
    {items = {"alchemie:alchemie_arnika_blossom"}, rarity = 1},
    {items = {"alchemie:alchemie_arnika_seed"}, rarity = 2},
  }
}
minetest.register_node("alchemie:arnika_4", table.copy(crop_def))

-- stage 5
crop_def.tiles = {"alchemie_arnika_5.png"}
crop_def.groups.growing = 0
crop_def.drop = {
  items = {
    {items = {"alchemie:alchemie_arnika_blossom"}, rarity = 1},
    {items = {"alchemie:alchemie_arnika_seed"}, rarity = 1},
  }
}
minetest.register_node("alchemie:arnika_5", table.copy(crop_def))

farming.registered_plants["alchemie:arnika"] = {
  crop = "alchemie:arnika",
  seed = "alchemie:alchemie_arnika_seed",
  minlight = 10,
  maxlight = 25,
  steps = 5
}

-- bonemeal
if minetest.get_modpath("bonemeal") then
	bonemeal:add_crop({
		{"alchemie:arnika_", 5},
	})
end
